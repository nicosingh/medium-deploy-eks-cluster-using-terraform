eks_pod_identity_associations = {
  "sample-apps-s3-readonly" = {
    policy_arn                = "arn:aws:iam::aws:policy/AmazonS3ReadOnlyAccess"
    service_account_name      = "s3-readonly"
    service_account_namespace = "sample-apps"
  }
}
